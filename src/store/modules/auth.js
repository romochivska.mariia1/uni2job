import { GoogleAuthProvider, signInWithPopup, signInWithCredential, signOut } from 'firebase/auth';
import { auth } from '@/firebase-config.js';
import Token from "@/token-usage.js";

export default {
  namespaced: true,
  state: {
    user: null,
    loading: false,
    error: null,
  },
  getters: {
    getUser: (state) => state.user,
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
  },
  actions: {
    async saveLoginUserData({ commit }, loginResult) {
      const user = loginResult?.user;
      commit('setUser', user);

      
      const tokenExpirationTime = new Date().getTime() + 30 * 60 * 1000;
      const token = user?.uid; 

      
      Token.setAccessTokenCookie(token, tokenExpirationTime);
      Token.setUserDataCookie(user);

      
    },

    loginWithGoogle({ commit, dispatch }) {
      const provider = new GoogleAuthProvider();
      signInWithPopup(auth, provider)
        .then((loginResult) => {
          dispatch('saveLoginUserData', loginResult);
        })
        .catch((error) => {
          commit('setError', error);
        });
    },

    async loginWithCredential({ commit, dispatch }) {
      return new Promise((resolve, reject) => {
        let credential = localStorage.getItem('authCredential');

        if (credential) {
          credential = JSON.parse(credential);
          credential = GoogleAuthProvider.credential(credential.idToken);

          signInWithCredential(auth, credential)
            .then((loginResult) => {
              dispatch('saveLoginUserData', loginResult);
              resolve(loginResult);
            })
            .catch((error) => {
              console.log(error);
              commit('setError', error);
              reject(false);
            });
        } else resolve(false);
      });
    },

    logout({ commit }) {
      signOut(auth)
        .then(() => {
          localStorage.removeItem('authCredential');
          Token.removeAccessTokenCookie();
          commit('setUser', null);
        })
        .catch((error) => {
          commit('setError', error);
        });
    },
  },
};