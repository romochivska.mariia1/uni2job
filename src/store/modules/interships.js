import getModuleSettingsObject from '../helpers/GetModuleSettingsObject'
export default {
    ...getModuleSettingsObject('interships'),
}
