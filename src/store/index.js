import { createStore } from 'vuex'
import auth from './modules/auth'
import interships from './modules/interships'
import users from './modules/users'
import jobs from './modules/jobs'

export default createStore({
    namespaced: true,
    modules: {
        interships,
        auth,
        jobs,
        users,
    },
    
})


