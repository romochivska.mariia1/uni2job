import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Token from "@/token-usage.js";

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/LoginPage.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import(/* webpackChunkName: "about" */ '../views/ProfilePage.vue')
  },
  {
    path: '/jobs',
    name: 'JobPage',
    component: () => import(/* webpackChunkName: "JobPage" */ '../views/JobPage.vue')
  },
  {
    path: '/interships',
    name: 'IntershipPage',
    component: () => import(/* webpackChunkName: "IntershipPage" */ '../views/IntershipPage.vue')
  },
  {
    path: '/jobAdd',
    name: 'jobAdd',
    component: () => import(/* webpackChunkName: "JobAdd" */ '../components/JobAdd.vue')
  },
  {
    path: '/interAdd',
    name: 'interAdd',
    component: () => import(/* webpackChunkName: "InterAdd" */ '../components/InterAdd.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  const accessToken = Token.getAccessTokenFromCookie();
  const nextRouteIsForLoggedIn = to.meta?.forLoggedIn;

  if (nextRouteIsForLoggedIn && !accessToken) {
    next("/login");
  } else if (to.name === "login" && accessToken) {
    next("/");
  } else {
    next();
  }
});

export default router;