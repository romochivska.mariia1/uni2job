// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore/lite'
import { getAuth } from 'firebase/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

const firebaseConfig = {
    apiKey: "AIzaSyBm-EwfSe-p9_qKEZM9TaD0z7YoEDIZceE",
    authDomain: "uni2job-fdcc5.firebaseapp.com",
    projectId: "uni2job-fdcc5",
    storageBucket: "uni2job-fdcc5.appspot.com",
    messagingSenderId: "277263541724",
    appId: "1:277263541724:web:aa43ec479d0cb383e91b19"
  };
  
// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
const db = getFirestore(app)
export default db